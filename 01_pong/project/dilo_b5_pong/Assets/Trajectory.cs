﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour{
    public BallControl ball;
    CircleCollider2D ballCollider;
    Rigidbody2D ballRigidbody;

    // projection ball at collision
    public GameObject ballAtCollision;

    // Start is called before the first frame update
    void Start(){
        ballRigidbody = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update(){
        bool drawBallAtCollision = false;

        // position to draw ballAtCollision
        Vector2 offsetHitPoint = new Vector2();

        // collision point from circle cast
        RaycastHit2D[] circleCastHit2DArray = Physics2D.CircleCastAll(ballRigidbody.position, ballCollider.radius, ballRigidbody.velocity.normalized);

        // for each collision point
        foreach (RaycastHit2D circleCastHit2D in circleCastHit2DArray){
            // if hit something but the ball itself
            if (circleCastHit2D.collider != null && circleCastHit2D.collider.GetComponent<BallControl>() == null){
                // trajectory drawn from ball's center point to ball's center point on hit
                // -> hit point. offset based on normal vector with ball's radius as size

                // hit point and its normal
                Vector2 hitPoint = circleCastHit2D.point;
                Vector2 hitNormal = circleCastHit2D.normal;

                // set offsetHitPoint, ball center point on hit
                offsetHitPoint = hitPoint + hitNormal * ballCollider.radius;

                // draw trajectory from ball's center point to ball's center point on hit
                DottedLine.DottedLine.Instance.DrawDottedLine(ball.transform.position, offsetHitPoint);

                // if not side walls, draw the reflection line
                if (circleCastHit2D.collider.GetComponent<SideWall>() == null){
                    // in and out vector
                    Vector2 inVector = (offsetHitPoint - ball.TrajectoryOrigin).normalized;
                    Vector2 outVector = Vector2.Reflect(inVector, hitNormal);

                    // Hitung dot product dari outVector dan hitNormal. Digunakan supaya garis lintasan ketika terjadi tumbukan tidak digambar.
                    // dot product from outVector and hitNormal -> what for? checked with and without the if bracket, not much changed
                    float outDot = Vector2.Dot(outVector, hitNormal);
                    if (outDot > -1.0f && outDot < 1.0){
                        // draw reflection trajectory
                        DottedLine.DottedLine.Instance.DrawDottedLine(offsetHitPoint, offsetHitPoint + outVector * 10.0f);

                        drawBallAtCollision = true;
                    }
                }

                // draw only for 1 reflection hit, break loop so dont calculate for more hit points
                break;                
            }
        }

        //draw shadow ball at collision handler
        if (drawBallAtCollision){
            ballAtCollision.transform.position = offsetHitPoint;
            ballAtCollision.SetActive(true);
        }
        else
            ballAtCollision.SetActive(false);
    }
}
