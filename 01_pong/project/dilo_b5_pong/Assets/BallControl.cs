﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour{
    // ball 2d rigidbody
    private Rigidbody2D rigidBody2D;

    // initial forces
    public float xInitialForce;
    public float yInitialForce;

    // ========== for debug
    private Vector2 trajectoryOrigin;

    // Start is called before the first frame update
    void Start(){
        rigidBody2D = GetComponent<Rigidbody2D>();
        RestartGame();

        // ========= for debug
        trajectoryOrigin = transform.position;
    }

    // Update is called once per frame
    void Update(){
    }

    // reset position and velocity to 0
    void ResetBall(){
        transform.position = Vector2.zero;
        rigidBody2D.velocity = Vector2.zero;
    }

    void PushBall(){
        // set random initial force and direction for ball
        float yRandomInitialForce = Random.Range(-yInitialForce, yInitialForce);
        float randomDirection = Random.Range(0, 2);

        // add force to ball, to left if direction is < 1, to right otherwise
        if (randomDirection < 1.0f)
            rigidBody2D.AddForce(new Vector2(-xInitialForce, yRandomInitialForce).normalized * xInitialForce);
        else
            rigidBody2D.AddForce(new Vector2(xInitialForce, yRandomInitialForce).normalized * xInitialForce);
    }

    // reset game, start game after 2 seconds delay
    void RestartGame(){
        ResetBall();
        Invoke("PushBall", 2);
    }

    // ========== for debug
    // record ball contact point on collision exit
    private void OnCollisionExit2D(Collision2D collision){
        trajectoryOrigin = transform.position;
    }

    public Vector2 TrajectoryOrigin { 
        get { return trajectoryOrigin; }
    }
}
